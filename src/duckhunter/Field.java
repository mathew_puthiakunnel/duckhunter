package duckhunter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
public class Field extends JPanel implements MouseListener {

    // instance variables
    public static Coordinate click;
    private String label; // message on the bottom of board
    private Ducks ducky;
    private int fieldWidth;
    private int fieldHeight;
    private int d; //remove this

    public Field() {
        click = null;
        label = " ";
        ducky = new Ducks();
        // set up the frame
        JFrame frame = new JFrame("Field");
        frame.setSize(716, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(this);
        frame.setVisible(true);
        this.addMouseListener(this);
        fieldWidth = frame.getWidth();
        fieldHeight = frame.getHeight();
    }

    @Override
    public void paintComponent(Graphics g) {
        drawBG(g);
        for (int i = 0; i < 200; i++) {
            ducky.drawDuck(g);
        }
        setLabel("Click to shoot. Shoot to win.", g);
    }

    public void drawBG(Graphics g) {
        Color sky = new Color(5, 125, 235);
        Color ground = new Color(95, 167, 67);

        g.setColor(sky);
        g.fillRect(0, 0, this.fieldWidth, this.fieldHeight);
        g.setColor(ground);
        g.fillRect(0, this.fieldHeight * 45 / 80, this.fieldWidth, this.fieldHeight * 35 / 80);
    }

    public void setLabel(String txt, Graphics g) {
        label = txt;
        g.setColor(Color.BLACK);
        g.drawString(label, 10, 600);
        repaint();
    }

    public Coordinate getClick() {
        click = null;
        while (click == null) {
            // wait for the click to happen
        }
        return click;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        System.out.println(x + " " + y);
        int col = x;
        int row = y;
        click = new Coordinate(row, col);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}