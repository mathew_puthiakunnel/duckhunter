package duckhunter;

public class Coordinate {

    private int row;
    private int col;
    private int d;// remove this..

    public Coordinate(int r, int c) {
        row = r;
        col = c;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public static boolean checkHit(Coordinate a, Coordinate b) {
        if (a.getRow() == b.getRow() && a.getCol() == b.getCol()) {
            return true;
        }
        return false;
    }
}
