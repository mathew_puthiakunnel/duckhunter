package duckhunter;

import java.awt.Color;
import java.awt.Graphics;

public final class Ducks {

    private boolean liveDuck;
    public Coordinate duckies;
    private float freq;
    public char type;
    private static final int RHN = 14344; //used to randomize duck types (RHN == Real High Number)
    public int row;
    public int col;
    private int d; //remove this..

    public Ducks() { //initialize ducks
        liveDuck = true;
        type = ' ';
        duckies = new Coordinate((int) (Math.random() * 700), (int) (Math.random() * 800));
        freq = (float) Math.random();
    }

    public void setType(int rand) { //set duck type
        boolean setNorm = (rand % 1 == 0) && !(rand % 2 == 0) && !(rand % 23 == 0);
        boolean setTwo = (rand % 2 == 0) && !(rand % 23 == 0);
        boolean setSpec = (rand % 453 == 0);
        System.out.println(rand);
        if (setNorm) {
            type = 'n'; // normal duck
        } else if (setTwo) {
            type = 'g'; // green duck
        } else if (setSpec) {
            type = 'f'; // flappy duck
        }
    }

    public void drawDuck(Graphics g) { //Show ducks on screen
        setType((int) (Math.random() * RHN));
        if (type == 'n') { //draw normal duck
            g.setColor(Color.red);
            g.fillRect(duckies.getRow(), duckies.getCol(), 75, 75);
        } else if (type == 'g') { //draw two duck
            g.setColor(Color.yellow);
            g.fillRect(duckies.getRow(), duckies.getCol(), 50, 50);
        } else if (type == 'f') { //draw flappy duck
            g.setColor(Color.green);
            g.fillRect(duckies.getRow(), duckies.getCol(), 25, 25);
        }
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }
}