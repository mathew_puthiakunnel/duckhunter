/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package duckhunter;

public class Player {

    public int score;
    private int mag_Size;
    public int bulletCount = mag_Size;
    public int avgHit = HitRate();
    private Ducks shot;
    private int d; //remove this..

    public boolean isDuckHit() { //check for shot ducks
        if (Coordinate.checkHit(shot.duckies, Field.click)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean Hit_or_Miss() { //used for score calculation
        if (isDuckHit()) {
            score++;
            bulletCount--;
            return true;
        } else {
            bulletCount--;
            return false;
        }
    }

    public int HitRate() { //calculates the player accuracy
        int shots_fired = mag_Size - bulletCount;
        return score / shots_fired;
    }
}
